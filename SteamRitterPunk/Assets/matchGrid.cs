﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class matchGrid : MonoBehaviour
{

    void Update()
    {
        transform.position = new Vector3(Mathf.Round(transform.position.x*2) / 2, Mathf.Round(transform.position.y*2) / 2, Mathf.Round(transform.position.z*2)/2);
        transform.localScale = new Vector3(Mathf.Round(transform.localScale.x), Mathf.Round(transform.localScale.y), Mathf.Round(transform.localScale.z));
    }
}
