﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraplingHook : MonoBehaviour {

    GameObject player;
    Rigidbody playerRigidbody;
    bool active = false;
    [SerializeField] float hookStrength;

	// Use this for initialization
	void Start () {
        Debug.Log("hook spawned");
        player = GameObject.FindGameObjectWithTag("Player");
        playerRigidbody = player.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if (active)
        {
            playerRigidbody.AddForce((transform.position-player.transform.position)*hookStrength);
        } else
        {
            transform.LookAt(transform.position + GetComponent<Rigidbody>().velocity);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hookable")
        {
            Debug.Log("graped");
            player.GetComponent<PlayerController>().Hooked = true;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            active = true;
        }
    }
}
