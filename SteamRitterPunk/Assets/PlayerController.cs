﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Rigidbody myRigidbody;
    Animator myAnimator;
    Camera mainCamera;

    [SerializeField] float startingHealth;
    [SerializeField] float health;

    [SerializeField] float movementPower;
    [SerializeField] float jumpPower;
    [SerializeField] float hookPower;
    [SerializeField] float shotPower;
    [SerializeField] bool hookShot = false;
    bool grounded;
    private bool hooked;

    [SerializeField] GameObject Pointer;
    [SerializeField] Transform graplingHook;
    [SerializeField] GameObject bullet;

    public bool Hooked
    {
        get
        {
            return hooked;
        }

        set
        {
            hooked = value;
        }
    }

    // Use this for initialization
    void Start () {
        myRigidbody = GetComponent<Rigidbody>();
        myAnimator = GetComponent<Animator>();
        //mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        mainCamera = Camera.main;

        health = startingHealth;
	}
	
	// Update is called once per frame
	void Update () {

        if(health <= 0)
        {
            Destroy(gameObject);
        }

        if (!hooked)
        {
            myRigidbody.velocity = new Vector3(Input.GetAxis("Horizontal") * movementPower, myRigidbody.velocity.y, 0);

            //update animator
            if (Input.GetAxis("Horizontal") == 0)
            {
                myRigidbody.velocity = new Vector3(0, myRigidbody.velocity.y, 0);
            }
        }




        if (grounded == true)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Debug.Log("Jump!");
                myRigidbody.AddForce(Vector3.up * jumpPower);
            }
        } 

        //graplinghook shoot
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 targetVector = mainCamera.ScreenToWorldPoint(Input.mousePosition - mainCamera.GetComponent<CameraController>().offset);
            Vector3 force = (targetVector - transform.position);
            force = new Vector3(force.x, force.y, 0);
            GameObject bul = Instantiate(bullet, transform.position + force.normalized*2, transform.rotation);
            
            bul.GetComponent<Rigidbody>().velocity =  force * shotPower;
        }

        //graplinghook
        if (Input.GetMouseButtonDown(1))
        {
            if (!hookShot)
            {
                Vector3 targetVector = mainCamera.ScreenToWorldPoint(Input.mousePosition - mainCamera.GetComponent<CameraController>().offset);
                Vector3 force = (targetVector - transform.position);
                force = new Vector3(force.x, force.y, 0);
                Transform hook = Instantiate(graplingHook, transform.position + force.normalized*1.5f, transform.rotation);
                hook.GetComponent<Rigidbody>().velocity = force  * hookPower;
                hookShot = true;

            }
            else
            {
                hookShot = false;
                Destroy(GameObject.FindGameObjectWithTag("GraplingHook"));
                hooked = false;
            }


            
        }

        grounded = false;
    }

    public void DealDamage(int dmg)
    {
        health -= dmg;

    }

    private void OnTriggerStay(Collider other)
    {
        grounded = true;
    }
}
