﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualSnowAdder : MonoBehaviour {

    [SerializeField] Texture resetTexture;
    [SerializeField] RenderTexture rt;
    [SerializeField] Material drawMat;

    private void Start()
    {
        Graphics.Blit(resetTexture, rt);
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, 256, 256), rt);
    }

    void Update () {
        if (Input.GetMouseButton(0))
        {
            Ray camray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(camray, out hit, 100);

            if (hit.collider == null)
            {
                return;
            }

            if (hit.collider.tag == "Snow")
            {

                RenderTexture temp = RenderTexture.GetTemporary(rt.width, rt.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(rt, temp);
                
                drawMat.SetVector("_uvPos",new Vector4(hit.textureCoord.x,hit.textureCoord.y,0,0));
                Graphics.Blit(temp, rt, drawMat);
                RenderTexture.ReleaseTemporary(temp);
            }
        }

    
	}
}
