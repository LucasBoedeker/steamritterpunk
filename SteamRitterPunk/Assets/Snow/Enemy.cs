﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] float startingHealth;
    [SerializeField] float health;


    [SerializeField] bool isActive;


    [Header("Targeting")]
    [SerializeField] bool targetLocked;
    [SerializeField] Transform target;
    [SerializeField] float deAggroDistance = 5;


    [Header("SearchField")]
    [SerializeField] Vector2 searchField;
    [SerializeField] float searchFieldRange;
    [SerializeField] float searchFieldSpeed;
    [SerializeField] float currentRotPercent;
    [SerializeField] float currentRotMultiplyer = 1;
    [SerializeField] Transform pointer;


    [Header("Shooting")]
    [SerializeField] float startingFireCD=1;
    [SerializeField] float fireCD;
    [SerializeField] float shootForce;
    [SerializeField] GameObject bullet;




    void Start () {
        transform.eulerAngles = new Vector3(searchField.x, -90, 0);
        health = startingHealth;
        fireCD = 0;
	}
	
	
	void Update () {

        if (health <= 0)
        {
            Destroy(this.gameObject);
        }

        if (isActive)
        {
            if (targetLocked)
            {
                CheckIfStillInRange();
                AimAtTarget();
                ShootIfPossible();
            }
            else
            {
                SearchFieldUpdate();
                SearchFieldCheck();
            }

        }
	}


    void AimAtTarget()
    {
        transform.LookAt(target);
       // transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, 0);
    }

    void CheckIfStillInRange()
    {
        float dis = Vector3.Distance(transform.position, target.position);

        if(dis> deAggroDistance)
        {
            targetLocked = false;
        }
    }

    void ShootIfPossible()
    {
        if(fireCD > 0)
        {
            fireCD -= Time.deltaTime;
        }
        else
        {
            fireCD = startingFireCD;
            Shoot();
        }

    }

    void Shoot()
    {
        GameObject obj = Instantiate(bullet, pointer.transform.position, Quaternion.identity);

        obj.GetComponent<Rigidbody>().AddForce((target.position - pointer.position) * shootForce);
        Debug.Log("Fire!");
    }

    void SearchFieldUpdate()
    {
        currentRotPercent += searchFieldSpeed * currentRotMultiplyer;
        if (currentRotPercent > 1)
        {
            currentRotPercent = 1;
            currentRotMultiplyer = -1;
        }

        if (currentRotPercent < 0)
        {
            currentRotPercent = 0;
            currentRotMultiplyer = 1;
        }

        transform.eulerAngles = new Vector3(Mathf.Lerp(searchField.x, searchField.y, currentRotPercent), -90,0);

    }

    void SearchFieldCheck()
    {
        Ray ray = new Ray(pointer.position, pointer.position - transform.position);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, searchFieldRange))
        {
            if (hit.transform.tag == "Player")
            {
                targetLocked = true;
                target = hit.transform;
            }
        }

        

    }

    public void DealDamage(int dmg)
    {
        health -= dmg;
    }
}
