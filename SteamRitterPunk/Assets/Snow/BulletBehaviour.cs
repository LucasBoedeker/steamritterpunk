﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {
    [SerializeField] float startingBulletLifetime;
     float bulletLifetime;
    [SerializeField] int dmg;

	void Start () {
        bulletLifetime = startingBulletLifetime;
	}
	

	void Update () {
        bulletLifetime -= Time.deltaTime;
        if(bulletLifetime <= 0)
        {
            Destroy(this.gameObject);
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        string tag = collision.transform.tag;
        if (tag == "Player" || tag == "Enemy")
        {
            if (tag == "Player")
            {
                collision.transform.GetComponent<PlayerController>().DealDamage(dmg);
            }

            if (tag == "Enemy")
            {
                collision.transform.GetComponent<Enemy>().DealDamage(dmg);
            }

            Destroy(this.gameObject);
        }
            }
}
