﻿Shader "LMJ/Snow1" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
		_Snow("Snow Texture",2D) = "white" {}
		_TexGround("Ground Texture",2D) = "white" {}
		_TexSnowTop("Snowtop Texture", 2D) = "white" {}
		_TexSnowTopMetallic("SnowTop Metallic/Gloss Texture",2D) = "white" {}

		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0

		_SnowMaxHeight("Snow Max Height", Range(0,100)) = 1

		_TessMinDist("Minimum Tesselation Distance", Range(0,100)) = 1
		_TessMaxDist("Maximum Tesselation Distance", Range(0,100)) = 10
		_Tess("Max Tessellation", Range(1,32)) = 4
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Standard fullforwardshadows tessellate:tess vertex:vert addshadow 

#pragma target 4.6
#include "Tessellation.cginc"

	struct Input {

		float2 uv_Snow;
		float2 uv_TexSnowTop;
		float2 uv_TexGround;
		float2 uv_TexSnowTopMetallic;

	};

	sampler2D _MainTex;

	sampler2D _Snow;

	sampler2D _TexGround;
	sampler2D _TexSnowTop;

	half _SnowMaxHeight;

	sampler2D _TexSnowTopMetallic;
	half _Glossiness;
	half _Metallic;

	half _TessMinDist;
	half _TessMaxDist;
	float _Tess;

	float4 tess(appdata_full v0, appdata_full v1, appdata_full v2) {
		return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, _TessMinDist, _TessMaxDist, _Tess);
	}


	void vert(inout appdata_full v) {

		half4 x = tex2Dlod(_Snow, v.texcoord);
		v.vertex.xyz += float3(0, x.x*x.w*_SnowMaxHeight , 0);
	}





	void surf(Input IN, inout SurfaceOutputStandard o) {
		
		half snowAmount = tex2D(_Snow, IN.uv_Snow).x;
		fixed4 snowTop = tex2D(_TexSnowTop, IN.uv_TexSnowTop);
		fixed4 ground = tex2D(_TexGround, IN.uv_TexGround);

		half modAmout = saturate(pow(snowAmount + 1, 3) - 1);
		half4 color = modAmout * snowTop + (1 - modAmout)*ground;
		o.Albedo = color.rgb;


		fixed4 metallicColor = tex2D(_TexSnowTopMetallic, IN.uv_TexSnowTopMetallic);
		o.Metallic = _Metallic*metallicColor.x*snowAmount;
		o.Smoothness = _Glossiness*metallicColor.y*snowAmount;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
