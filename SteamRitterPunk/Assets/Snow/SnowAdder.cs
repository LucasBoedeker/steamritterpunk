﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowAdder : MonoBehaviour {

    [SerializeField] RenderTexture rt;
    [SerializeField] Material drawMat;


   
    private void OnCollisionStay(Collision coll) { 
        
            Ray ray = new Ray(transform.position, Vector3.down);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 2);

            RenderTexture temp = RenderTexture.GetTemporary(rt.width, rt.height, 0, RenderTextureFormat.ARGBFloat);
            Graphics.Blit(rt, temp);

            drawMat.SetVector("_uvPos", new Vector4(hit.textureCoord.x, hit.textureCoord.y, 0, 0));
            Graphics.Blit(temp, rt, drawMat);
            RenderTexture.ReleaseTemporary(temp);
        
    }
}
